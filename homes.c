#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "homes.h"

// TODO: Write a function called readhomes that takes the filename
// of the homes file and returns an array of pointers to home structs
// terminated with a NULL
struct home ** readhomes(char * filename){  
      
    FILE * f = fopen(filename,"r");
         
    int size = 50;
    
    struct home ** homearray;
    homearray = (struct home **)malloc(size * sizeof(struct home *));
    
    
    for( int k = 0; k < size; k++){   
        homearray[k] =(struct home *)malloc(sizeof(struct home));
        homearray[k]->addr = (char *)malloc(50);
    }
    
    int i = 0;
    int j = size;   
        
    while( fscanf(f,"%d,%[^,],%d,%d\n",       &homearray[i]->zip,
                                               homearray[i]->addr,
                                              &homearray[i]->price,
                                              &homearray[i]->area) != EOF){
        if( i == (size - 1)){
            size += 50;
            struct home ** newarray = (struct home **)realloc(homearray,size * sizeof(struct home *));
            homearray = newarray;
            while( j < size){
                homearray[j] = (struct home *)malloc(sizeof(struct home));
                homearray[j]->addr = (char *)malloc(50);
                j++;
            }
       }
       
       i++; 
    }    
    fclose(f);
    return homearray;
}

int numhomes(struct home ** arrayname)
{
    int length = 0;  
    while(arrayname[length] != NULL){
        length++;
    }   
    return length;
}

int samezip(struct home ** arrayname, int zipcode)
{
    int count = 0;
    int i = 0;
    while(arrayname[i] != NULL){
        if(arrayname[i]->zip == zipcode){
            count++;
        }
        i++;
    }
    return count;
}

int sameprice(struct home ** arrayname, int price)
{
    int count = 0;
    int i = 0;
    while(arrayname[i] != NULL){
        if(arrayname[i]->price - price < 10000 && price - arrayname[i]->price < 10000)
            count++;
        i++;
    }
    return count;
}

int main(int argc, char *argv[])
{
    
    // TODO: Use readhomes to get the array of structs
       
    struct home ** array = readhomes("listings.csv");

    // At this point, you should have the data structure built
    // TODO: How many homes are there? Write a function, call it,
    // and print the answer.

    int number = numhomes(array);
    
    printf("There are %d homes in the list.\n", number);
    
    // TODO: Prompt the user to type in a zip code.
    
    int zipcode = 0;

    printf("Please enter a zip code: ");
    scanf("%d",&zipcode);
    
    // At this point, the user's zip code should be in a variable
    // TODO: How many homes are in that zip code? Write a function,
    // call it, and print the answer.

    int zips = samezip(array,zipcode);
    
    printf("There are %d homes with the same zipcode as yours.\n",zips);
           
    // TODO: Prompt the user to type in a price.
    
    int price = 0;
    printf("Please enter a price: $");
    scanf("%d",&price);
    
    // At this point, the user's price should be in a variable
    // TODO: Write a function to print the addresses and prices of 
    // homes that are within $10000 of that price.
    
    int numprices = sameprice(array,price);
    printf("There are %d homes with the same price.\n",numprices);
    
    int i = 0;
    while( array[i] != NULL){
        free(array[i]->addr);
        free(array[i]);
        i++;
    }
    free(array);
    
    return 0;
}
